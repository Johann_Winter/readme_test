# Burnt Books

Der Sinn der App ist es, die durch die Nazis verbrannten Bücher für die Recherche 
verfügbar zu machen. Die Bücherliste kann durchsucht werden, Favoriten gespeichert werden und
Verfügbarkeit der Bücher in Bibliotheken angezeigt werden.

* Version 1.0


## Installation

* Einfach die APK installieren bzw. im Emulator starten.


## Todos

* "Favorite"-Funktionalität fixen: App stürzt ab bei Tap auf "Browse Favorites" im Hauptmenü, und bei Tap auf das Herz bei einzelnen Einträgen der Liste (favorisieren).
* Hauptmenü (LandingPage) aufhübschen zB. mit Bildern oder Imagebuttons
* Bei Tap auf einen Eintrag in der Bücherliste Öffnen der Detailansicht


## Autor

* **Tom B.**


## Screenshots

** Hauptmenü:**

![hauptmenue.png](https://bitbucket.org/repo/npnjL8/images/1480858547-hauptmenue.png)

** Übersicht:**

![uebersicht.png](https://bitbucket.org/repo/npnjL8/images/2577269401-uebersicht.png)

** Details: (Beispiel)**

![detailansicht.png](https://bitbucket.org/repo/npnjL8/images/3676870506-detailansicht.png)